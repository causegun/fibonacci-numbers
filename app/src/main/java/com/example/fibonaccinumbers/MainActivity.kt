package com.example.fibonaccinumbers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private val fibonacciSequence = mutableListOf(0, 1)
    private var currentFibonacciValue = 1
    private var isCalculating = false
    private val scope = CoroutineScope(Dispatchers.Main)

    private lateinit var startButton: Button
    private lateinit var fibonacciEditText: EditText
    private lateinit var fibonacciValueTextView: TextView
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_button)
        fibonacciEditText = findViewById(R.id.edit_text)
        fibonacciValueTextView = findViewById(R.id.fibonacci_value_text_view)

        startButton.setOnClickListener {
            if (!isCalculating) {
                val input = fibonacciEditText.text.toString().toIntOrNull()
                if (input != null && input > 0) {
                    startCalculation(input)
                }
            } else {
                stopCalculation(true)
            }
        }

        if (savedInstanceState != null) {
            currentFibonacciValue = savedInstanceState.getInt("currentFibonacciValue")
            isCalculating = savedInstanceState.getBoolean("isCalculating")
            savedInstanceState.getIntegerArrayList("fibonacciSequence")
                ?.let { fibonacciSequence.addAll(it) }
            updateValueTextView()
        }
    }

    private fun startCalculation(input: Int) {
        isCalculating = true
        startButton.text = getString(R.string.cancel)
        fibonacciEditText.isEnabled = false

        fibonacciSequence.clear()
        fibonacciSequence.addAll(listOf(0, 1))

        job = scope.launch {
            while (currentFibonacciValue < input) {
                fibonacciSequence.add(fibonacciSequence[currentFibonacciValue - 1] + fibonacciSequence[currentFibonacciValue])
                currentFibonacciValue++
                updateValueTextView()
                delay(1000)
            }
            // Calculation is complete, update UI with the final value
            currentFibonacciValue = input
            stopCalculation()
        }
    }

    private fun stopCalculation(isStopForced: Boolean = false) {
        job.cancel()
        isCalculating = false
        currentFibonacciValue = 1
        scope.launch {
            startButton.text = getString(R.string.start)
            fibonacciEditText.isEnabled = true
        }
        if (isStopForced) {
            updateValueTextView().cancel()
            fibonacciValueTextView.text = ""
        } else updateValueTextView()
    }

    private fun updateValueTextView() =
        scope.launch {
            fibonacciValueTextView.text = if (isCalculating)
                getString(R.string.current_fibonacci, fibonacciSequence[currentFibonacciValue])
            else
                getString(R.string.result_fibonacci, fibonacciSequence.last())
        }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentFibonacciValue", currentFibonacciValue)
        outState.putBoolean("isCalculating", isCalculating)
        outState.putIntegerArrayList("fibonacciSequence", ArrayList(fibonacciSequence))
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentFibonacciValue = savedInstanceState.getInt("currentFibonacciValue")
        isCalculating = savedInstanceState.getBoolean("isCalculating")
        savedInstanceState.getIntegerArrayList("fibonacciSequence")
            ?.let { fibonacciSequence.addAll(it) }
        updateValueTextView()
    }
}