# Fibonacci Numbers

The task is to add a screen with a simple UI that has one button, EditText, and a text field.

This screen has a few states:

- A user can enter a series of the Fibonacci numbers and press the "Start" button. After pressing this button, the calculation should start and the "Start" button should be changed to "Cancel". EditText is non-active.
- The value of a text field is updated every second with the current calculated value. The "Cancel" button is active and EditText is non-active.
- The final calculated value is displayed in the text field. The button has the "Start" state, EditText is active, and the user can enter a new value.

The calculation process shouldn't be disabled after the configuration is changed.
